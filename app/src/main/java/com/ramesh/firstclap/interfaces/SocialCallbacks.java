package com.ramesh.firstclap.interfaces;

import com.ramesh.firstclap.model.User;

/**
 * Created by TGT on 11/8/2017.
 */

public interface SocialCallbacks {
  public void success(User user);
}
