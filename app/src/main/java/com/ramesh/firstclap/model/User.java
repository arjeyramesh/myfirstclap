package com.ramesh.firstclap.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by TGT on 11/8/2017.
 */

public class User {
  @SerializedName("firstname")
  private String firstname;
  @SerializedName("lastname")
  private String lastname;
  @SerializedName("email")
  private String email;
  @SerializedName("user_id")
  private String id;
  @SerializedName("message")
  private String message;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  @SerializedName("code")
  private int code;
  @SerializedName("phonenumber")
  private String phonenumber;
  @SerializedName("aboutme")
  private String aboutme;
  @SerializedName("location")
  private String location;
  @SerializedName("locationid")
  private String locationid;
  private String providerid;
  private String accesstoken;
  private String deviceid = "";
  private String devicemodel = "";

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  @SerializedName("image")
  private String image;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }




  public String getProviderId() {
    return providerid;
  }

  public void setProviderId(String provider) {
    this.providerid = provider;
  }

  public String getAccesstoken() {
    return accesstoken;
  }

  public void setAccesstoken(String accesstoken) {
    this.accesstoken = accesstoken;
  }


  public String getDevicemodel() {
    return devicemodel;
  }

  public void setDevicemodel(String devicemodel) {
    this.devicemodel = devicemodel;
  }

  public String getDeviceOs() {
    return deviceOs;
  }

  public void setDeviceOs(String deviceOs) {
    this.deviceOs = deviceOs;
  }

  public String getDevicetype() {
    return devicetype;
  }

  public void setDevicetype(String devicetype) {
    this.devicetype = devicetype;
  }

  private String deviceOs = "";
  private String devicetype = "";

  public String getDeviceid() {
    return deviceid;
  }

  public void setDeviceid(String deviceid) {
    this.deviceid = deviceid;
  }


  public String getProfession() {
    return profession;
  }

  public void setProfession(String profession) {
    this.profession = profession;
  }

  @SerializedName("profession")
  private String profession;


  private static User sSoleInstance;

  public static User getInstance() {
    if (sSoleInstance == null) { //if there is no instance available... create new one
      sSoleInstance = new User();
    }

    return sSoleInstance;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }


  public String getAboutme() {
    return aboutme;
  }

  public void setAboutme(String aboutme) {
    this.aboutme = aboutme;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLocationid() {
    return locationid;
  }

  public void setLocationid(String locationid) {
    this.locationid = locationid;
  }


  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String toJSONString() {
    JSONObject parentobject = new JSONObject();
    JSONObject userobject = new JSONObject();
    JSONObject userdeviceobject = new JSONObject();
    try {
      userobject.put("FirstName", firstname);
      userobject.put("LastName", lastname);
      userobject.put("Email", email);
      userobject.put("Token", accesstoken);
      userobject.put("ImageUrl", image);
      userobject.put("ProviderId", providerid);
      userdeviceobject.put("DeviceId", deviceid);
      userdeviceobject.put("Type", devicetype);
      userdeviceobject.put("OS", deviceOs);
      userdeviceobject.put("Model", devicemodel);
      parentobject.put("User", userobject);
      parentobject.put("device", userdeviceobject);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return parentobject.toString();
  }

  public String toSetupProfileJsonString() {
    HashMap<String, String> map = new HashMap<>();
    map.put("email", email);
    map.put("firstname", firstname);
    map.put("lastname", lastname);
    map.put("phonenumber", phonenumber);
    map.put("location", location);
    map.put("aboutme", aboutme);
    map.put("profession", profession);
    return new JSONObject(map).toString();
  }


}
