package com.ramesh.firstclap;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.ramesh.firstclap.service.FeedService;
import com.ramesh.firstclap.service.UserService;

import au.com.dstil.atomicauth.IAuthFailureCallback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by TGT on 11/9/2017.
 */

public class MyFirstClapApplication extends Application implements IAuthFailureCallback {

    protected MyFirstClapApplication application;
    private static final int RC_SIGN_IN_FB = 1916;
    public UserService userService;
    public FeedService feedService;
    ProgressDialog dialog;

    // Updated your class body:
    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the SDK before executing any other operations,
        facebookInit();
        retrofitInit();


    }

    public MyFirstClapApplication returnInstance() {
        return application;
    }

    public void facebookInit() {
        FacebookSdk.sdkInitialize(getApplicationContext(), RC_SIGN_IN_FB);
        AppEventsLogger.activateApp(this);

    }

    @Override
    public void onAuthFailure(String s) {

    }

    public void retrofitInit() {
        Retrofit api = new Retrofit
                .Builder()
                .baseUrl(getResources().getString(R.string.api_base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        userService = new UserService(api);
        feedService = new FeedService(api);


    }


    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
}
