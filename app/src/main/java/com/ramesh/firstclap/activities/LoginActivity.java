package com.ramesh.firstclap.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.ramesh.firstclap.MyFirstClapApplication;
import com.ramesh.firstclap.R;
import com.ramesh.firstclap.controllers.FacebookLoginController;
import com.ramesh.firstclap.controllers.GoogleController;
import com.ramesh.firstclap.controllers.UserLoginController;
import com.ramesh.firstclap.interfaces.SocialCallbacks;
import com.ramesh.firstclap.model.User;
import com.ramesh.firstclap.utils.Utils;

import au.com.dstil.atomicauth.callback.FailureCallback;
import au.com.dstil.atomicauth.callback.SuccessCallback;

import static com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable;

/**
 * Created by TGT on 11/28/2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener,
        SocialCallbacks, FailureCallback<String>, GoogleController.GetRuntimePermissionscallback,
        GoogleApiClient
                .ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
  private ImageView signinFacebook;
  private ImageView signinGoogle;
  private LoginButton loginbuttonFacebook;
  private FacebookLoginController facebookcontroller;
  private CallbackManager callbackManager;
  GoogleController googlecontroller;
  private GoogleApiClient mGoogleApiClient;
  private ConnectionResult connection_result;
  private boolean is_intent_inprogress;
  private boolean is_signInBtn_clicked;
  protected MyFirstClapApplication application;
  Utils utils;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    buidNewGoogleApiClient();
    super.onCreate(savedInstanceState);
    application = (MyFirstClapApplication) getApplication();
    callbackManager = CallbackManager.Factory.create();
    setContentView(R.layout.activity_main);
    init();

  }

  private void init() {
    utils = new Utils(LoginActivity.this);
    signinFacebook = (ImageView) findViewById(R.id.signin_facebook);
    signinGoogle = (ImageView) findViewById(R.id.signin_google);
    loginbuttonFacebook = (LoginButton) findViewById(R.id.loginbutton_facebook);
    facebookcontroller = new FacebookLoginController(LoginActivity.this, loginbuttonFacebook, callbackManager);
    googlecontroller = new GoogleController(LoginActivity.this, mGoogleApiClient, this);
    signinFacebook.setOnClickListener(this);
    signinGoogle.setOnClickListener(this);


  }


  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.signin_facebook:
        loginbuttonFacebook.performClick();
        facebookcontroller.facebookLogin(this);
        break;
      case R.id.signin_google:
        gPlusSignIn();
        break;
    }

  }


  @Override
  protected void onActivityResult(int reqCode, int resCode, Intent i) {
    if (reqCode == 0) {
      googlecontroller.onActivityResult(reqCode, resCode, i);
    } else {
      callbackManager.onActivityResult(reqCode, resCode, i);
    }
  }


  @Override
  public void success(User user) {
    /*


    Bundle bundle = new Bundle();
    bundle.putString("firstname", user.getFirstname());
    bundle.putString("lastname", user.getLastname());
    bundle.putString("email", user.getEmail());
    Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
    intent.putExtras(bundle);
    startActivity(intent);*/

    User finaluserObject = getDeviceDetails(user);
    if (application.isConnected()) {
      showDialog();
      UserLoginController.loginUser(application, finaluserObject, new SuccessCallback<User>() {
        @Override
        public void success(User user) {
          hideDialog();
        }
      }, this);
    } else {
      hideDialog();
    }
  }


  @Override
  public void getRuntimePermissions(int request_code_ask_permissons) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

      int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
      if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
        requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                request_code_ask_permissons);
        return;
      } else {
        requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                request_code_ask_permissons);
        return;
      }
    } else {
      googlecontroller.getProfileInfo();
    }
  }

  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

    //Checking the request code of our request
    if (requestCode == 123) {
      //If permission is granted
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        googlecontroller.getProfileInfo();
      } else {
        googlecontroller.deniedPermissonError();
      }
    }
  }

  public void buidNewGoogleApiClient() {
    mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(Plus.API, Plus.PlusOptions.builder().build())
            .addScope(Plus.SCOPE_PLUS_PROFILE)
            .addScope(Plus.SCOPE_PLUS_LOGIN)
            .build();
  }


  @Override
  public void onConnected(Bundle bundle) {
  }

  @Override
  public void onConnectionSuspended(int i) {
  }

  @Override
  public void onConnectionFailed(ConnectionResult result) {
    // Always assign result first
    connection_result = result;

    if (!result.hasResolution()) {
      isGooglePlayServicesAvailable(LoginActivity.this);
      return;
    }
    if (!is_intent_inprogress) {
      if (is_signInBtn_clicked) {
        // The user has already clicked 'sign-in' so we attempt to
        // resolve all
        // errors until the user is signed in, or they cancel.
        resolveSignInError();
      }
    }
  }

  protected void onStart() {
    super.onStart();
    mGoogleApiClient.connect();
  }

  protected void onStop() {
    super.onStop();
    if (mGoogleApiClient.isConnected()) {
      mGoogleApiClient.disconnect();
    }
  }

  protected void onResume() {
    super.onResume();

    if (mGoogleApiClient.isConnected()) {
      mGoogleApiClient.connect();
    }
  }

  public void gPlusSignIn() {
    if (!mGoogleApiClient.isConnecting()) {
      Log.d("user connected", "connected");
      resolveSignInError();

    }
  }

  private void resolveSignInError() {
    if (connection_result.hasResolution()) {
      try {
        is_intent_inprogress = true;
        connection_result.startResolutionForResult(this, 0);
        Log.d("resolve error", "sign in error resolved");
      } catch (IntentSender.SendIntentException e) {
        is_intent_inprogress = false;
        googlecontroller.googleClientConnectionStarted();
      }
    }
  }

  @Override
  public void failure(String s) {
    hideDialog();

  }


  public User getDeviceDetails(User user) {
    String deviceUniqueIdentifier = null;
    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
    if (null != tm) {
     /* deviceUniqueIdentifier = tm.getDeviceId();*/
      user.setDevicetype("Phone");
      deviceUniqueIdentifier = "1345dftyghui";
    }
    if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
      deviceUniqueIdentifier = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
      user.setDevicetype("Tablet");
    }
    String model = Build.MODEL;
    String versionRelease = Build.VERSION.RELEASE;


    user.setDeviceid(deviceUniqueIdentifier);
    user.setDevicemodel(model);
    user.setDeviceOs(versionRelease);
    return user;
  }
}

