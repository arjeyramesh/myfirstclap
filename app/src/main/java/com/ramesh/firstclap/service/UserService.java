package com.ramesh.firstclap.service;

import com.ramesh.firstclap.model.User;

import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.util.List;

import au.com.dstil.atomicauth.annotation.Authentication;
import au.com.dstil.atomicauth.callback.DeferredRestCallback;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import static au.com.dstil.atomicauth.annotation.Authentication.Type.OAUTH;

/**
 * Created by TGT on 11/28/2017.
 */

public class UserService {
    private final ServiceDefinition service;

    public UserService(Retrofit retrofit) {
        service = retrofit.create(ServiceDefinition.class);
    }


    public Call<User> loginUser(final User user) {
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), user.toJSONString());
        final Call<User> call = service.login(requestBody);
        return call;
    }


    public Call<User> setupProfile(final User user) {
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), user.toSetupProfileJsonString());
        final Call<User> call = service.setupProfile(requestBody);
        return call;
    }

    interface ServiceDefinition {
        @Authentication(OAUTH)
        @POST("login.php")
        Call<User> login(@Body RequestBody user);

        @Authentication(OAUTH)
        @POST("setupprofile")
        Call<User> setupProfile(@Body RequestBody user);

        @Authentication(OAUTH)
        @GET("users/{id}/userdetails")
        Call<List<UserService>> get(@Path("id") int id);
    }
}
