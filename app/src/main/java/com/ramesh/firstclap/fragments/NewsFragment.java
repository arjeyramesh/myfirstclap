package com.ramesh.firstclap.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ramesh.firstclap.BuildConfig;
import com.ramesh.firstclap.MyFirstClapApplication;
import com.ramesh.firstclap.R;
import com.ramesh.firstclap.adapters.NewsEndLessAdapter;
import com.ramesh.firstclap.controllers.FeedController;
import com.ramesh.firstclap.interfaces.FacebookFeedsCallback;
import com.ramesh.firstclap.model.FacebookFeed;
import com.ramesh.firstclap.utils.EndlessListView;

/**
 * Created by TGT on 12/13/2017.
 */

public class NewsFragment extends Fragment implements FacebookFeedsCallback, EndlessListView.EndLessListener {
    EndlessListView listview;
    protected MyFirstClapApplication application;
    FeedController feedcontroller;
    NewsEndLessAdapter adapter;
    String aftercursor;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        application = (MyFirstClapApplication) getActivity().getApplication();
        init(view);
        return view;
    }

    public void init(View v) {
        listview = (EndlessListView) v.findViewById(R.id.listview);
        listview.setLoadingView(R.layout.loading_layout);

        //	scrollview listener
        listview.setListener(this);
        FeedController.getFeeds(application, BuildConfig.Indie_Wire_PageId, this);

    }


    @Override
    public void successCallBack(FacebookFeed facebookfeed) {


        com.ramesh.firstclap.model.FacebookFeed.Paging paging = facebookfeed.getPaging();
        com.ramesh.firstclap.model.FacebookFeed.Cursors cursors = paging.getCursors();


        aftercursor = cursors.getAfter();
        if (adapter == null) {
            adapter = new NewsEndLessAdapter(getActivity(), facebookfeed.getData(), R
                    .layout.news_list_item);
            listview.setAdapter(adapter);
        } else {
            listview.addNewData(facebookfeed.getData());
        }


    }

    @Override
    public void failureCallback(String failureexception) {

    }

    @Override
    public void loadData() {
        /*FeedController.getFeeds(application, BuildConfig.Indie_Wire_PageId, this);*/


        /*FeedController.loadMoreFeeds(application, BuildConfig.Indie_Wire_PageId, aftercursor, this);
*/
    }
}
