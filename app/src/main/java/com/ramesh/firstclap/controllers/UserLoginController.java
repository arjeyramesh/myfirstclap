package com.ramesh.firstclap.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.ramesh.firstclap.MyFirstClapApplication;
import com.ramesh.firstclap.model.User;

import au.com.dstil.atomicauth.callback.FailureCallback;
import au.com.dstil.atomicauth.callback.SuccessCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TGT on 11/28/2017.
 */

public final class UserLoginController {

  public static void loginUser(final MyFirstClapApplication application, final User user, final SuccessCallback<User> successcallback, final FailureCallback<String> failureCallback) {

    application.userService.loginUser(user).enqueue(new Callback<User>() {
      @Override
      public void onResponse(Call<User> call, Response<User> response) {
        User user = response.body();
        successcallback.success(user);
      }

      @Override
      public void onFailure(Call<User> call, Throwable t) {
        failureCallback.failure(t.toString());
      }
    });


  }

  public static void setUpProfile(MyFirstClapApplication application, final User user, final SuccessCallback<User> successCallback, final FailureCallback<String> failureCallback) {
    if (application.isConnected()) {
      application.userService.setupProfile(user).enqueue(new Callback<User>() {
        @Override
        public void onResponse(Call<User> call, Response<User> response) {

        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {

        }
      });

    }
  }
}
