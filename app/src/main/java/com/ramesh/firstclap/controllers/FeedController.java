package com.ramesh.firstclap.controllers;

import com.ramesh.firstclap.MyFirstClapApplication;
import com.ramesh.firstclap.interfaces.FacebookFeedsCallback;
import com.ramesh.firstclap.model.FacebookFeed;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TGT on 12/13/2017.
 */

public final class FeedController {


    public static void getFeeds(MyFirstClapApplication application, String pageid, final
    FacebookFeedsCallback facebookfeedsCallback) {
        application.feedService.getFeeds(pageid).enqueue(new Callback<FacebookFeed>() {
            @Override
            public void onResponse(Call<FacebookFeed> call, Response<FacebookFeed> response) {
                FacebookFeed facebookfeed = response.body();


                facebookfeedsCallback.successCallBack(facebookfeed);
            }

            @Override
            public void onFailure(Call<FacebookFeed> call, Throwable t) {

            }
        });
    }

    public static void loadMoreFeeds(MyFirstClapApplication application, String pageid, String
            aftercursor, final
                                     FacebookFeedsCallback facebookfeedsCallback) {
        application.feedService.loadMoreFeeds(pageid, aftercursor).enqueue(new Callback<FacebookFeed>() {
            @Override
            public void onResponse(Call<FacebookFeed> call, Response<FacebookFeed> response) {
                FacebookFeed facebookfeed = response.body();
                facebookfeedsCallback.successCallBack(facebookfeed);
            }

            @Override
            public void onFailure(Call<FacebookFeed> call, Throwable t) {

            }
        });
    }
}
